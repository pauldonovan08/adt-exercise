<html>
 <head>
  <title>Cash Register</title>

  <link href="/css/bootstrap.min.css" rel="stylesheet" />
  <link href="/css/app.css" rel="stylesheet" />
 </head>
 
 <body>
    <nav class="navbar navbar-dark bg-dark">
        <span style="padding-left:50px;" class="navbar-brand">Cash Register</span>
    </nav>
    <div class="container">
        <form method="post" action="/index.php">
            <div class="row row-buffer">
                <label for="totalAmount" class="col-sm-2"> Total Amount </label> </span> 
                <div class="col-sm-10">
                    <input class="form-control" type="text" id="totalAmount" name="totalAmount" />
                </div>
            </div>
            <div class="row row-buffer">
                <label for="tenderedAmount" class="col-sm-2"> Tendered Amount </label> 
                <div class="col-sm-10">
                    <input class="form-control" type="text" id="tenderedAmount" name="tenderedAmount" /> 
                </div>
            </div>
            <div class="row row-buffer">
                <input class="btn btn-primary" type="submit" value="Get Change">
            </div>
        </form>


        <?php 
            include 'validation.php';
            include 'denominations.php';
            include 'CalculateChangeServiceClient.php';

            if ($_SERVER["REQUEST_METHOD"] == "POST") {
                
                $total_amount = $_POST['totalAmount'];
                $tendered_amount = $_POST['tenderedAmount'];

                //Validate inputs
                if(!validate_currency($total_amount) || !validate_currency($tendered_amount)){
                    echo '<p> Invalid currency value, please check your inputs! </p>';
                    return;
                }
                if(!validate_currency($total_amount) || !validate_currency($tendered_amount)){
                    echo '<p> Invalid currency value, please check your inputs! </p>';
                    return;
                }
                $total_amount_numeric = round(floatval($total_amount),2 );
                $tendered_amount_numeric = round(floatval($tendered_amount), 2);
                if($tendered_amount_numeric < $total_amount_numeric){
                    echo "<p> You're short! Please tender more cash. </p>";
                    return;
                }
                //End validation


                //Call change calculation service via client class
                $calculate_client = new CalculateChangeServiceClient("https://us-central1-adt-code-exercise.cloudfunctions.net");
                $calculation_results_json = $calculate_client->call_service($tendered_amount_numeric, $total_amount_numeric);                
                
                echo '<p>$' . number_format($total_amount, 2) . ' out of $' . number_format($tendered_amount,2) . ", the customer's change is $" . 
                    number_format($tendered_amount_numeric-$total_amount_numeric, 2) . ".</p>";
            
                if($tendered_amount_numeric-$total_amount_numeric > 0){
                    
                    echo '<p> Please give the customer ';

                    $instructions = array();
                    foreach( $denominations as &$denomination ){
                        $denom_id = $denomination["id"];
                        if(isset($calculation_results_json->$denom_id)){
                            $singular_or_plural = ($calculation_results_json->$denom_id > 1) ? "plural" : "singular";
                            $instructions[] = $calculation_results_json->$denom_id . " " . $denomination[$singular_or_plural];
                        }
                    }
                        
                    $tmp = array_keys($instructions);
                    $last_key = end($tmp);
                    reset($instructions);

                    foreach($instructions as $key => $instruction) {
                        if ( $key == $last_key){
                            echo "and " . $instruction . ".</p>";
                        }
                        else{
                            echo $instruction . ", ";
                        }
                    }
                }
            }
            ?>

    </div>
 </body>
 <script src="/js/bootstrap.min.js"></script>
</html>