<?php

    $denominations = array(
        array("id"=>"$20", "singular"=>"Twenty Dollar Bill", "plural"=> "Twenty Dollar Bills"),
        array("id"=>"$10", "singular"=>"Ten Dollar Bill", "plural"=> "Ten Dollar Bills"),
        array("id"=>"$5", "singular"=>"Five Dollar Bill", "plural"=> "Five Dollar Bills"),
        array("id"=>"$1", "singular"=>"One Dollar Bill", "plural"=> "One Dollar Bills"),
        array("id"=>"Quarter", "singular"=>"Quarter", "plural"=> "Quarters"),
        array("id"=>"Dime", "singular"=>"Dime", "plural"=> "Dimes"),
        array("id"=>"Nickel", "singular"=>"Nickel", "plural"=> "Nickels"),
        array("id"=>"Penny", "singular"=>"Penny", "plural"=> "Pennies")
    );

?>