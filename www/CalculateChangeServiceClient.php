<?php
class CalculateChangeServiceClient
{
    protected string $base_url;

    public function __construct(string $base_url){
        $this->base_url = $base_url; 
    }

    // Call the calculate change service and return the result (or error)
    public function call_service($tendered_amount_numeric, $total_amount_numeric) {
        $post_data = array('tenderedAmount'=>floatval($tendered_amount_numeric), 'totalAmount'=>$total_amount_numeric);
        $url =  $this->base_url . '/Calculate_Change';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        $result = curl_exec($ch);

        if (curl_errno($ch)) { 
            return curl_error($ch); 
        } 

        curl_close($ch);

        $results_json = json_decode($result);

        return $results_json;
    }
}
?>