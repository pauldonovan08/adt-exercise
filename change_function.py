available_denominations = [
    {
        "name": "$20",
        "value": 20
    },
    {
        "name": "$10",
        "value": 10
    },
    {
        "name": "$5",
        "value": 5
    },
    {
        "name": "$1",
        "value": 1
    },
    {
        "name": "Quarter",
        "value": .25
    },
    {
        "name": "Dime",
        "value": .10
    },
    {
        "name": "Nickel",
        "value": .05
    },
    {
        "name": "Penny",
        "value": .01
    }
]

def whole_numbers_money(amount):
    return round(amount * 100)

def calculate_change(request):
    request_json = request.get_json()
    if request_json and 'tenderedAmount' in request_json:
        change_amount = round(request_json['tenderedAmount'] - request_json['totalAmount'], 2)
        change_denominations = {}

        print(request_json['tenderedAmount'])
        print(request_json['totalAmount'])

        print(change_amount)

        change_left = whole_numbers_money(change_amount)

        for denomination in available_denominations:
            denomination_name = denomination["name"]
            denomination_amount = whole_numbers_money(denomination["value"])
            if change_left // denomination_amount > 0:
                change_denominations[denomination_name] = change_left // denomination_amount
                change_left = change_left % denomination_amount

        return change_denominations
    else:
        return f'Missing something.'


#___________Local Unit Testing___________________________________________________________________________________
class Request:

    def __init__(self):
        self.args = []

    def get_json(self):
        return {
            "totalAmount": 40.63,
            "tenderedAmount": 60.00
        }


print (calculate_change(Request()))